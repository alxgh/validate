let assert = require('assert');
let validate = require('../src')

describe('String rule', () => {
    it('It accepts empty string as string.', () => {
        const errors = validate({
            'p': 'string',
        }, {
            p: ''
        });

        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });
});