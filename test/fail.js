let assert = require('assert');
let validate = require('../src')

describe('Arrays', () => {
    it('It should fail when there is an error.', () => {
        const failedValidation = validate({'p': 'string'}, {p: 123});

        assert.equal(failedValidation.failed, true);
    });

    it('It not should fail when there is no error.', () => {
        const failedValidation = validate({'p': 'string'}, {p: '123'});

        assert.equal(failedValidation.failed, false);
    });
});