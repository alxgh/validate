let assert = require('assert');
let validate = require('../src')

describe('Range rule', () => {
    it('It should return error when number is out of range.', () => {
        const lessThanMinimum = validate({'p': 'number|range:3,8'}, {p: 2});
        const higherThanMaximum = validate({'p': 'number|range:3,8'}, {p: 9});

        assert.equal(lessThanMinimum.errors.hasOwnProperty('p'), true);
        assert.equal(higherThanMaximum.errors.hasOwnProperty('p'), true);
    });

    it('It should not return error when number is minimum or maximum of range.', () => {
        const equalToMinimum = validate({'p': 'number|range:3,8'}, {p: 3});
        const equalToMaximum = validate({'p': 'number|range:3,8'}, {p: 8});

        assert.equal(equalToMinimum.errors.hasOwnProperty('p'), false);
        assert.equal(equalToMaximum.errors.hasOwnProperty('p'), false);
    });

    it('It should not return error when number is in range.', () => {
        const inRange = validate({'p': 'number|range:3,8'}, {p: 4});

        assert.equal(inRange.errors.hasOwnProperty('p'), false);
    });
});