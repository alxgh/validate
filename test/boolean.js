let assert = require('assert');
let validate = require('../src')

describe('Boolean rule', () => {
    it('It should accept true as boolean.', () => {
        const errors = validate({
            'p': 'boolean',
        }, {
            p: true
        });

        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });

    it('It should accept false as boolean.', () => {
        const errors = validate({'p': 'boolean', }, {p: false});

        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });

    it('It should not accept string as boolean.', () => {
        const aString = validate({'p': 'boolean'}, {p: 'string'});

        assert.equal(aString.errors.hasOwnProperty('p'), true);
    });

    it('It should not accept \'1\' and \'0\' (strings) in strict mode as boolean.', () => {
        const oneAsString = validate({'p': 'boolean'}, {p: '1'});
        const zeroAsString = validate({'p': 'boolean'}, {p: '0'});

        assert.equal(oneAsString.errors.hasOwnProperty('p'), true);
        assert.equal(zeroAsString.errors.hasOwnProperty('p'), true);
    });

    it('It should accept \'1\' and \'0\' (strings) in non strict mode as boolean.', () => {
        const oneAsString = validate({'p': 'boolean:true'}, {p: '1'});
        const zeroAsString = validate({'p': 'boolean:true'}, {p: '0'});

        assert.equal(oneAsString.errors.hasOwnProperty('p'), false);
        assert.equal(zeroAsString.errors.hasOwnProperty('p'), false);
    });

    it('It should not accept \'true\' and \'false\' (strings) in strict mode as boolean.', () => {
        const oneAsString = validate({'p': 'boolean'}, {p: 'true'});
        const zeroAsString = validate({'p': 'boolean'}, {p: 'false'});

        assert.equal(oneAsString.errors.hasOwnProperty('p'), true);
        assert.equal(zeroAsString.errors.hasOwnProperty('p'), true);
    });

    it('It should accept \'true\' and \'false\' (strings) in non strict mode as boolean.', () => {
        const oneAsString = validate({'p': 'boolean:true'}, {p: 'true'});
        const zeroAsString = validate({'p': 'boolean:true'}, {p: 'false'});

        assert.equal(oneAsString.errors.hasOwnProperty('p'), false);
        assert.equal(zeroAsString.errors.hasOwnProperty('p'), false);
    });
})