let assert = require('assert');
let validate = require('../src')

describe('Custom valdiators', () => {
    it('It should return error if custom validator fails.', () => {
        const customValidatorFails = validate({'p': [() => {return { result: false, message: 'Message'}}]}, {p: []});

        assert.equal(customValidatorFails.errors.hasOwnProperty('p'), true);
        assert.equal(customValidatorFails.errors.p, 'Message');
    });

    it('It should not return error if custom validator passes.', () => {
        const customValidatorFails = validate({'p': [() => {return { result: true, message: 'Message'}}]}, {p: []});

        assert.equal(customValidatorFails.errors.hasOwnProperty('p'), false);
    });
});