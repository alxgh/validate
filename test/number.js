let assert = require('assert');
let validate = require('../src')

describe('Number rule', () => {
    it('It should accept integer as number', () => {
        const errors = validate({'p': 'number'}, {p: 123});

        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });

    it('It should accept floats as number', () => {
        const errors = validate({'p': 'number',}, {p: 123.23});
        
        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });

    it('It should not accept strings as number', () => {
        const errors = validate({'p': 'number',}, { p: '123.23'});

        assert.equal(errors.errors.hasOwnProperty('p'), true);
    });
});