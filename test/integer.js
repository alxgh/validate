let assert = require('assert');
let validate = require('../src')

describe('Integer rule', () => {
    it('It should accept integer as integer.', () => {
        const errors = validate({'p': 'integer'}, {p: 123});

        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });

    it('It should not accept floats as integer.', () => {
        const errors = validate({'p': 'integer',}, {p: new Number(123.23)});

        assert.equal(errors.errors.hasOwnProperty('p'), true);
    });

    it('It should not accept strings as integer.', () => {
        const errors = validate({'p': 'integer',}, { p: '123.23'});

        assert.equal(errors.errors.hasOwnProperty('p'), true);
    });
});