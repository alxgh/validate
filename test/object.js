let assert = require('assert');
let validate = require('../src')
let dot = require('dot-object')
describe('Objects', () => {
    it('It should not return error for object\'s properties if object is not included.', () => {
        const emptyArray = validate({'p': 'object', 'p.name': 'string'}, {});

        assert.equal(dot.pick('p.name', emptyArray.errors) !== undefined, false);
    });

    it('It should return error for object\'s properties if object is included.', () => {
        const notEmptyArray = validate({'p': 'object', 'p.name': 'string'}, {p: {}});

        assert.equal(dot.pick('p.name', notEmptyArray.errors) !== undefined, true);
    });

    it('It should not return error if object\'s property is optional and its not included.', () => {
        const notEmptyArray = validate({'p': 'object', 'p.name?': 'string'}, {p: {}});

        assert.equal(dot.pick('p.name', notEmptyArray.errors) !== undefined, false);
    });

    it('It should return error if object\'s property is optional but it is included.', () => {
        const notEmptyArray = validate({'p': 'object', 'p.name?': 'string'}, {p: {name: 12}});

        assert.equal(dot.pick('p.name', notEmptyArray.errors) !== undefined, true);
    });
});