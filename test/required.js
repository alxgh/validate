/**
 * Test's for required rule.
 */

let assert = require('assert');
let validate = require('../src')

describe('Required rule', function() {
    it('It should return error if property is not included.', function() {
        const errors = validate({
            'p': 'string',
        }, {
        });
        assert.equal(errors.errors.hasOwnProperty('p'), true);
    });

    it('It should not return error if property is not included.', function() {
        const errors = validate({
            'p': 'string',
        }, {
            p: 's'
        });
        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });

    it('It should not return error if property is not included and it is optional.', function() {
        const errors = validate({
            'p?': 'string',
        }, {
        });
        assert.equal(errors.errors.hasOwnProperty('p'), false);
    });
})