let assert = require('assert');
let validate = require('../src')
let dot = require('dot-object');

describe('Arrays', () => {
    it('It should not return error for its indexes rules when array is empty.', () => {
        const emptyArray = validate({'p': 'array', 'p.*.name': 'string'}, {p: []});

        assert.equal(dot.pick('p.0.name', emptyArray.errors) !== undefined, false);
    });

    it('It should return error for its indexes rules when array is not empty.', () => {
        const notEmptyArray = validate({'p': 'array', 'p.*.name': 'string'}, {p: [{}]});

        assert.equal(dot.pick('p.0.name', notEmptyArray.errors) !== undefined, true);
    });

    it('It should return error for every index.', () => {
        const invalidIndexes = validate({'p': 'array', 'p.*.name': 'string'}, {p: [{}, {}]});

        assert.equal(dot.pick('p.0.name', invalidIndexes.errors) !== undefined, true);
        assert.equal(dot.pick('p.1.name', invalidIndexes.errors) !== undefined, true);
    });

    it('It should only return error for invalid indexes.', () => {
        const arrayValidation = validate({'p': 'array', 'p.*.name': 'string'}, {p: [{}, {name: 'string'}]});

        assert.equal(dot.pick('p.0.name', arrayValidation.errors) !== undefined, true);
        assert.equal(dot.pick('p.1.name', arrayValidation.errors) !== undefined, false);
    });
});