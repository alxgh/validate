Request validator
=================
This package is useful for validating request bodies.

NOTE: All fields are reqired by default.

Existsing rules:
----------------

#### Optional property:
Put `?` at the end of property name.
```js
'property?' : 'string'
```

#### Integer:
```js
'property' : 'integer'
```

#### Number: (any kind of number)
```js
'property' : 'number'
```

#### String:
```js
'property' : 'string'
```

#### Array:

```js
'property' : 'array'
```

#### Object:
```js
'property' : 'object'
```

#### Email:
```js
'property' : 'email'
```

#### Length:

```js
'property' : 'length:(max|min),(int)'
```

```js
'property' : 'length:max:50',
```

#### Size:

```js
'property' : 'length:(max|min),(int)'
```

#### Range (inclusive):

```js
'property' : 'range:(min),(max)'
```
```js
'property' : 'range:5,10'
```

#### Boolean:

It has a strict mode and non strict mode.

In strict mode it just accepts `true` and `false`.

In non strict mode it accepts `'true'`, `'false'`, `1`, `0`, `'1'` and `'0'`.

```js
'property' : 'boolean:(strict)'
```

```js
'property' : 'boolean' // strict mode
```

```js
'property' : 'boolean:true' // non strict mode
```

#### Custom validator:

It should return the result of validation and error message.
Error message is used if result is `false`.

```js
'property': [() =>{
  return {
    result: true,
    message: "return error message"
  }
}]
```

#### Using multiple validation rules:

Seprate them by `|` :

```js
'property': 'number|range:4,8'
```

Pass rules as array:

```js
'property': ['number', 'range:4,8']
```

#### Validate object's properties:

```js
'property.child': 'number',
```

### Validate array indexes:

Every index should be a number:

```js
'property.*': 'number'
```

Every index is a object which has a name and an age:

```js
'property': 'array':
'property.*.name': 'string|length:max,50',
'property.*.age': 'number|range:18,150',
```

Example
-------

```js
const validate = require('@alxgh/validate');

const {errors, output, failed} = validate({
    'username': 'string|length:min,4|length:max,50',
    'password': 'string|length:min,6',
    'last_name': 'string|length:max,50',
    'first_name': 'string|length:max,50',
    'role?': 'integer',
}, input);
if(failed) {
  // Validaton has failed
  // use the `errors`
} else {
  // Use the `output`. it does not contain extra fields.
}
```

In above example the object should have `username`, `password`, `last_name`, `first_name` and `role` which is optional.