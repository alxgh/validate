const dot = require('dot-object');
/**
 * 
 * @param {String} string String to be trimmed.
 * @param {String} charlist List of characters to be trimmed.
 */
const trim = (string, charlist) => {
    return string.replace(new RegExp("^[" + charlist + "]+"), "").replace(new RegExp("[" + charlist + "]+$"), "");
}

const validator = {};
/**
 * Check if the field is integer. 
 */
validator.integer = ({ input }) => {
    return typeof input === 'number' && ! Number.isNaN(Number.parseInt(input)) && Number.parseInt(input) === Number.parseFloat(input);
}
/**
 * Check if the field is number. 
 */
validator.number = ({ input }) => {
    return typeof input === 'number' && ! Number.isNaN(Number.parseFloat(input));
}
/**
 * Check if the field is string. 
 */
validator.string = ({ input }) => {
    return typeof input === 'string';
}
/**
 * Check if the field is array. 
 */
validator.array = ({ input }) => {
    return Array.isArray(input);
}
/**
 * Check if the field is object. 
 */
validator.object = ({ input }) => {
    return typeof input === 'object';
}
/**
 * Checks the property exists in the input. 
 */
validator.required = ({data, field}) => {
    return dot.pick(field, data) !== undefined;
}
/**
 * Length validator.
 */
validator.length = ({input, ruleParams}) => {
    const type = ruleParams[0];
    const length = ruleParams[1];
    const inputLength = input.length;

    return type === 'min' ? inputLength >= length : inputLength <= length;
}
/**
 * Valdiate the size of the array.
 */
validator.size = ({input, ruleParams}) => {
    return validator.length({input, ruleParams});
}
/**
 * Range validator for numbers.
 */
validator.range = ({input, ruleParams}) => {
    const min = ruleParams[0];
    const max = ruleParams[1];

    return input >= min && input <= max;
}
/**
 * Validate email.
 */
validator.email = ({ input }) => {
    let regex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

    return regex.test(input);
}

/**
 * Boolean validator.
 */
validator.boolean = ({input, ruleParams}) => {
    // Accepts 0,1 and string false, true in non strict mdoe.
    const nonStrict = ruleParams[0] == '1' || ruleParams[0] == 'true'  ? true :false;
    
    if(nonStrict) {
        return (
            input === true
            || input === false
            || input === '1'
            || input === '0'
            || input === 1
            || input === 0
            || input === 'true'
            || input === 'false'
        );
    } else {
        return input === true || input === false;
    }
}
/**
 * This prevents further validations.
 * 
 * Usefull when a param is not required so it wont fail on it's validation rules when not included.
 */
validator.optional = (inp) => {
    return validator.required(inp);
}
/**
 * List of errors.
 * 
 * `undefined` Indicates no error. It's used in optional rule.
 */
validator.errors = {
    'integer': '%field must be integer.',
    'number': '%field must be number',
    'string': '%field must be string.',
    'array': '%field must be array.',
    'required': '%field is required.',
    'length': '%field %0 length must be %1.',
    'object': '%field must be object.',
    'optional': undefined,
    'range': 'Range of %field is from %0 to %1',
    'size': '%field size must be %0.',
    'boolean': '%field must be boolean.',
    'email': '%field must be a valid email address',
}

/**
 * Validate given data based on schema.
 * @param {*} schema 
 * @param {*} data 
 * 
 * @returns {*} List of errors
 */
validator.validate = (schema, data) => {
    // List of errors.
    const errors =  {};
    // Output object containing validated fields.
    const output = {};
    // List of optional fields.
    // It is used to check if a field has been declared as an optional field.
    const optionals = [];

    // Refactor validation rules.
    // It implies * rules which is used for arrays to every exists index.
    const entries = Object.entries(schema).reduce((acc, [field, rules]) => {
        // Split the field by '*'.
        const startSplitted = field.split('*');
        // If it's not a array child field just pass.
        if(startSplitted.length === 1) {
            acc.push([field, rules]);
            return acc;
        }

        // Iterate through array in a recursive way to discover last depth of field.
        const _l = (splitted, data, name, depth = 0) => {
            const arr = dot.pick(trim(name, '.'), data) || [];
            // If it's not final depth iterate once more,
            if(depth < splitted.length - 1) {
                if(Array.isArray(arr)) {
                    arr.map((_, i) => {
                        _l(splitted, data, trim(name, '.') + `.${i}.` + trim(splitted[depth + 1], '.'), depth + 1);
                    });
                }
            } else {
                // If we are at the bottom of field depth push it to rules.
                v = [trim(name, '.'), rules];
                acc.push(v);
            }
        }
        _l(startSplitted, data, startSplitted[0]);
        return acc;
    }, []);
    // console.log(entries)
    Object.values(entries).map(([field, rules]) => {
        if(! Array.isArray(rules)) {
            rules = rules.split('|');
        }
        // Remove '?' from end of field name. '?' indicates whether field is required or not.
        const name = field.replace('?', '');
        // Field is required if a '?' is not included as the last character of the name.
        const required = field.substr(-1) !== '?';
        // If field is required add required rule to the rules list
        if(required) {
            // Check parent exists and its not optional.
            let parent = name.split('.');
            parent.pop();
            // If one of the parents are optional and it is not included then skip this.
            const parentCheck = parent.reduce((acc, curr, _, arr) => {
                // Push the current parent to list.
                acc.identifier.push(curr);
                // Check if field is optional and it is not included in given data to validate.
                if(dot.pick(acc.identifier.join('.'), data) === undefined) {
                    arr.slice(1);
                    acc.result = false;
                }
                return { ...acc };
            }, {result: true, identifier: []});
            // If parent does exists
            if(parentCheck.result) {
                rules = ['required', ...rules];
            } else {
                rules = ['optional', ...rules];
            }
        } else {
            // Add optional rule.
            rules = ['optional', ...rules];
            // optionals.push(name);
        }

        const validatorInput = {input: dot.pick(name, data), data, field: name};
        dot.copy(name, name, data, output);
        
        // Validator goes on till first error.
        rules.some((rule) => {
            // Check if the rule is a string, So it calls built in validators.
            if(typeof rule === 'string') {
                // Check validator if exists.
                const ruleName = rule.split(':')[0];

                if(! validator.hasOwnProperty(ruleName)) {
                    console.log(rule)
                    throw Error('Invalid rule.');
                }
                const ruleParams = rule.split(':').pop().split(',');
                // Call the validator.
                // It passes whole data, field name and value.
                const result = validator[ruleName]({...validatorInput, ruleParams});
                // If result is false it means validation has failed so we add the error and stop the validation for this field.
                if(! result) {
                    let message = validator.errors[ruleName];
                    if(message) {
                        message = message.replace('%field', name);
                        // Replace validation rule inputs in the error message.
                        ruleParams.forEach((param, c) => {
                            message = message.replace(`%${c}`, param);
                        });
                    
                        dot.set(name, message, errors);
                    }
                    return true;
                }
            } else if(rule instanceof Function) {
                // If rule is a function validator calls it.
                // The function should return a object containing the result and error message.
                // {
                //     result: false,
                //     message: 'whatever it has to be.'
                // }

                const { result, message } = rule(validatorInput);

                // Stop if validation has failed.
                if(! result) {
                    dot.set(name, message, errors)
                    return true;
                }
            } else {
                // The rule is not valid. It's neither a string (built in) or a funciton.
                throw Error('Invalid rule.');
            }
        })
    });

    return { 
        errors,
        output,
        failed: Object.entries(errors).length > 0,
    };
}

module.exports = validator.validate;